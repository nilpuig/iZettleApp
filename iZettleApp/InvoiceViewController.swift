//
//  invoiceViewController.swift
//  iZettleApp
//
//  Created by Haley on 11/7/17.
//  Copyright © 2017 Nil. All rights reserved.
//

import UIKit
import iZettle

class invoiceViewController: UIViewController {

    @IBOutlet weak var payBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set horizontal and vertical position of invoice labels
        var yPos = 100
        let xPos = 60
        
        // Create title labels for the invoice
        let name = UILabel(frame: CGRect(x: xPos-35, y: yPos, width: 100, height: 40))
        name.text = "Product"
        name.font = UIFont.boldSystemFont(ofSize: 16)
        
        let quantity = UILabel(frame: CGRect(x: xPos+100, y: yPos, width: 100, height: 40))
        quantity.text = "Quantity"
        quantity.font = UIFont.boldSystemFont(ofSize: 16)
        
        let price = UILabel(frame: CGRect(x: xPos+200, y: yPos, width: 100, height: 40))
        price.text =  "Price"
        price.font = UIFont.boldSystemFont(ofSize: 16)
        
        self.view.addSubview(name)
        self.view.addSubview(quantity)
        self.view.addSubview(price)
        
        // If there isn't any item in checkout, show "No items yet" message
        if (checkout.cartList.count == 0) {
            yPos += 40
            let name = UILabel(frame: CGRect(x: xPos-35, y: yPos, width: 100, height: 40))
            name.text = "No items yet"
            name.font = UIFont.systemFont(ofSize: 15)
            self.view.addSubview(name)
            
            // Hide the pay button, we can't pay if there are no items
            payBtn.isHidden = true
            return
        }
        
        // Create a list of 3 colums with the products on the cart (i.e. product name, quantity and price)
        for product in checkout.cartList {
            yPos += 40

            let name = UILabel(frame: CGRect(x: xPos-35, y: yPos, width: 150, height: 70))
            name.text = product.id
            name.numberOfLines = 3
            name.font = UIFont.systemFont(ofSize: 15)
            
            let quantity = UILabel(frame: CGRect(x: xPos+128, y: yPos, width: 100, height: 70))
            quantity.text = String(product.quantity)
            quantity.font = UIFont.systemFont(ofSize: 15)
            
            let price = UILabel(frame: CGRect(x: xPos+200, y: yPos, width: 100, height: 70))
            price.text = iZettle.currency! + " " + String(product.price * Double(product.quantity)) 
            price.font = UIFont.systemFont(ofSize: 15)
            
            self.view.addSubview(name)
            self.view.addSubview(quantity)
            self.view.addSubview(price)
        }
        print ("Total Price:")
        print (checkout.invoice())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        
        // 'Pay' button is pressed
        if (sender.tag == 1) {
            // All paid, so set all product quantities to 0
            for product in checkout.cartList {
                product.quantity = 0
            }
            checkout.cartList.removeAll()
        }
        
        // If the 'arrow' button is pressed, the product quantities are conserved, so in the next check out the current products will be in the next bill as well
        
        // Go home screen
        performSegue(withIdentifier: "goHome", sender: nil)
        
    }

}
