//
//  modifiersViewController.swift
//  iZettleApp
//
//  Created by Haley on 11/7/17.
//  Copyright © 2017 Nil. All rights reserved.
//

import UIKit
import iZettle

class modifiersViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBOutlet weak var sauce1: UISwitch!
    @IBOutlet weak var sauce2: UISwitch!
    @IBOutlet weak var sauce3: UISwitch!
    @IBOutlet weak var option1: UISegmentedControl!
    // Function called when user clicks on 'continue' or 'continue without modifiers' button
    @IBOutlet weak var option2: UISegmentedControl!
    
    @IBAction func goToBill(_ sender: UIButton) {
        
        // If user presses Continue button, do the following
        if (sender.tag == 0) {
            
            /// Get the selected modifiers names
            let modifierName1 = option1.titleForSegment(at: option1.selectedSegmentIndex)
            let modifierName2 = option2.titleForSegment(at: option2.selectedSegmentIndex)
            
            // Get the selected sauce names
            var souceName = ""
            if (sauce1.isOn) {
                souceName += "Ketchup "
            }
            if (sauce2.isOn) {
                souceName += "Mayonnaise "
            }
            if (sauce3.isOn) {
                souceName += "Vinegar "
            }
            
            // Create the menu
            checkout.addMenuToCart(mainProduct: iZettle.productListArray.last!, modifierName: [modifierName1!, modifierName2!], optionName: souceName)
            
        }
        else {
            // Add the burger menu product to the cart only if it hasn't been added before (i.e. quantity is 0)
            if (iZettle.productListArray[iZettle.productListArray.count-1].quantity < 1) {
                checkout.cartList.append(iZettle.productListArray[iZettle.productListArray.count-1])
            }
            
            // Increase the quantity of the selected product
            iZettle.productListArray[iZettle.productListArray.count-1].quantity += 1
        }
        
        performSegue(withIdentifier: "goToBill", sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
