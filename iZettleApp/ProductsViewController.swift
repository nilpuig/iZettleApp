//
//  productsViewController.swift
//  iZettleApp
//
//  Created by Haley on 11/7/17.
//  Copyright © 2017 Nil. All rights reserved.
//

import UIKit
import iZettle

class productsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Only create products the first time the app is loaded
        if (iZettle.productsCreated == false) {
            createProducts()
            iZettle.productsCreated = true
        }
        
    }
    
    // Create products
    func createProducts() {
    // Set up desired currency
        iZettle.currency = "£"
        
        // Create the products of our app with the framework
        let chips = Product(id: "Chips", price: 5, priceWithMenu:4)
        let wedges = Product(id: "Potato wedges", price: 5.0,priceWithMenu:4)
        let sweet = Product(id: "Ice cream", price: 4.5,priceWithMenu:4)
        let juice = Product(id: "Juice", price: 2.5,priceWithMenu:2)
        let nuggets = Product(id: "Nuggets", price: 2.8,priceWithMenu:2)
        let water = Product(id: "Water", price: 1.5,priceWithMenu:1)
        let soda = Product(id: "Soda", price: 1.8,priceWithMenu:1)
        let burger = Product(id: "Burger", price: 9.5, hasModifiers:true)
        
        // Add the products to an array in the framework to ease the access to the products
        iZettle.productListArray = [nuggets, sweet, soda, chips, wedges, water, juice, burger]
}
    
// Function called when user clicks on a product
    @IBAction func productSelection(_ sender: UIButton) {
        
        // If the product doesn't have modifiers, go to invoice view controller directly
        if (iZettle.productListArray[sender.tag].hasModifiers) {
            performSegue(withIdentifier: "goToModifers", sender: nil)
        }
        else {
            // Add the selected product to the cart only if it hasn't been added before (i.e. quantity is 0)
            if (iZettle.productListArray[sender.tag].quantity < 1) {
                checkout.cartList.append(iZettle.productListArray[sender.tag])
            }
            
            // Increase the quantity of the selected product
            iZettle.productListArray[sender.tag].quantity += 1
            
            print (iZettle.productListArray[sender.tag].quantity)
            
            performSegue(withIdentifier: "goToBill", sender: nil)
        }
        
    }
}

